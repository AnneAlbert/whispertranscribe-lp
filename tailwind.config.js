/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./**/*.{html,js}"],
  theme: {
    screens: {
      sm: '480px',
      md: '768px',
      mdd: '833px',
      lg: '976px',
      header: '1002px',
      pricing: '1088px',
      xl: '1440px',
      
    },
    extend: {
      colors: {
        brightRed: 'hsl(12, 88%, 59%)',
        brightRedLight: 'hsl(12, 88%, 69%)',
        brightRedSupLight: 'hsl(12, 88%, 95%)',
        darkBlue: 'hsl(228, 39%, 23%)',
        darkGrayishBlue: 'hsl(227, 12%, 61%)',
        veryDarkBlue: 'hsl(233, 12%, 13%)',
        veryPaleRed: 'hsl(13, 100%, 96%)',
        veryLightGray: 'hsl(0, 0%, 98%)',
        mainPurple: 'hsl(257, 88%, 71%)', 
        logoPurple: 'hsl(260, 50%, 49%)',
        purpleDark: 'hsl(260, 79%, 66%)',
        purpleDarkHover: 'hsl(260, 69%, 59%)',
        darkPurple: 'hsl(260, 75%, 57%)',
      },
      fontFamily: {
        'otomanopee': ["'Otomanopee One'", 'sans-serif']
      },
    },
  },
  plugins: [
    require('tailwindcss-patterns'),
    require('@tailwindcss/typography'),

  ],
}